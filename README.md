# LunchScraper
Simple script for scraping today's lunch menu from webpage of Huvila restaurant.

Browse to http://lounas.dyndns-home.com/huvila/ for output.

# Pre-requirements

* Nginx web server
* UWsgi emperor 
* Python 2.7 
* Virtualenv

# Installation

Install Nginx. Refer to Uwsgi documentation to install uwsgi as emperor. 
Create symbolic links for scraper.conf and scraper.ini to /etc/nginx/sites-enabled and /etc/uwsgi/vassals, respectively.