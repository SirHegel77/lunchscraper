#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask
import requests
import locale
import calendar
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import logging


logger = logging.getLogger(__name__)

FORMAT = '%(asctime)-15s %(levelname)s: %(message)s'
logging.basicConfig(format=FORMAT)

DAYS = ['MAANANTAI', 'TIISTAI', 'KESKIVIIKO',
    'TORSTAI', 'PERJANTAI', 'LAUANTAI', 'SUNNUNTAI']

app = Flask(__name__)

@app.route('/huvila/')
def huvila():
    """
    Scrape today's lunch from webpage of Huvila restaurant.
    """

    logger.info("Scraping lunch menu from Huvila website")

    today = datetime.today() 
    if today.weekday() > 4:
        # No lunch on weekends
        return "Nyt on viikonloppu,<br> tänään ei ole lounasta."
    
    # Read lunch menu from webpage
    r = requests.get('http://www.huvilassa.fi/lounas/')
    
    # Create parser
    bs = BeautifulSoup(r.content, 'html.parser')

    # Compose header to find on page
    header = "| {} {}.{} |".format(DAYS[today.weekday()], today.day, today.month)
    
    # Find today's menu
    title = bs.find('strong', text=header)
    container = title.find_parent('div')
    ps = container.find_all('p')

    # Compose message
    result = u"<p><strong>Huvilassa</strong> on lounaana tänään:<p>"
    for p in ps:
        if p.text <> header:
            p['style']='' # Remove style information
            result += u"{}".format(p)

    return result

if __name__ == '__main__':
    print huvila()
